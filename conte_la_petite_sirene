LA PETITE SIRÈNE.


Bien loin dans la mer, l’eau est bleue comme les feuilles des bluets,
pure comme le verre le plus transparent, mais si profonde qu’il serait
inutile d’y jeter l’ancre, et qu’il faudrait y entasser une quantité
infinie de tours d’églises les unes sur les autres pour mesurer la
distance du fond à la surface.

C’est là que demeure le peuple de la mer. Mais n’allez pas croire
que ce fond se compose seulement de sable blanc ; non, il y croît
des plantes et des arbres bizarres, et si souples, que le moindre
mouvement de l’eau les fait s’agiter comme s’ils étaient vivants.
Tous les poissons, grands et petits, vont et viennent entre les
branches comme les oiseaux dans l’air. À l’endroit le plus profond
se trouve le château du roi de la mer, dont les murs sont de corail,
les fenêtres de bel ambre jaune, et le toit de coquillages qui
s’ouvrent et se ferment pour recevoir l’eau ou pour la rejeter.
Chacun de ces coquillages referme des perles brillantes dont la
moindre ferait honneur à la couronne d’une reine.

Depuis plusieurs années le roi de la mer était veuf, et sa vieille
mère dirigeait sa maison. C’était une femme spirituelle, mais si
fière de son rang, qu’elle portait douze huîtres à sa queue tandis
que les autres grands personnages n’en portaient que six. Elle
méritait des éloges pour les soins qu’elle prodiguait à ses six
petites filles, toutes princesses charmantes. Cependant la plus
jeune était plus belle encore que les autres ; elle avait la peau
douce et diaphane comme une feuille de rose, les yeux bleus comme
un lac profond ; mais elle n’avait pas de pieds : ainsi que ses
sœurs, son corps se terminait par une queue de poisson.

Toute la journée, les enfants jouaient dans les grandes salles du
château, où des fleurs vivantes poussaient sur les murs. Lorsqu’on
ouvrait les fenêtres d’ambre jaune, les poissons y entraient comme
chez nous les hirondelles, et ils mangeaient dans la main des
petites sirènes qui les caressaient. Devant le château était un
grand jardin avec des arbres d’un bleu sombre ou d’un rouge de feu.
Les fruits brillaient comme de l’or, et les fleurs, agitant sans
cesse leur tige et leurs feuilles, ressemblaient à de petites
flammes. Le sol se composait de sable blanc et fin, et une lueur
bleue merveilleuse, qui se répandait partout, aurait fait croire
qu’on était dans l’air, au milieu de l’azur du ciel, plutôt que
sous la mer. Les jours de calme, on pouvait apercevoir le soleil,
semblable à une petite fleur de pourpre versant la lumière de son
calice.
