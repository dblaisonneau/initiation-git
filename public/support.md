```
document: conte-le-pecheur-et-le-geant.txt
version: 0001
```

![version1](version1.png)

<div style="page-break-after: always;"></div>

```
document: conte-le-pecheur-et-le-geant.txt
version: 0002
```

![version2](version2.png)

<div style="page-break-after: always;"></div>

```
document: conte-le-pecheur-et-le-geant.txt
version: 0003
```

![version3](version3.png)

<div style="page-break-after: always;"></div>

```
différence entre la version: 0001 et 0002
```
![diff1](diff1.png)

```
différence entre la version: 0002 et 0003
```
![diff2](diff2.png)


<div style="page-break-after: always;"></div>

### correction du lieu d'échouage

```
@@ -12,7 +12,7 @@ C'était une carte au trésor, il décida donc d'aller le chercher.
 
 Il prit son bateau et s'en alla en mer avec de quoi se nourrir et
 boire durant plusieurs mois. Un jour, il commença à manquer de
-nourriture. Le pêcheur dut s'échouer sur une CHOUCROUTE.
+nourriture. Le pêcheur dut s'échouer sur une île.
 
 Cette île avait une atmosphère étrange. Des milliers de vautours
 survolaient l'île. Il y avait aussi un brouillard épais. Il regarda la
```
### ajout des auteurs

```
@@ -26,4 +26,6 @@ Le géant le mangea. C'est ainsi que le pêcheur mourut.
 
 Ne vous fiez jamais aux apparences, le pêcheur est mort en
 croyant que cette clé était merveilleuse. Les hommes sont
-souvent aveuglés par la richesse et la cupidité.
\ No newline at end of file
+souvent aveuglés par la richesse et la cupidité.
+
+Auteurs: Lisa Orange et Damien Dennemont
```

### ajouts des auteurs

```
@@ -28,4 +28,4 @@ Ne vous fiez jamais aux apparences, le pêcheur est mort en
 croyant que cette clé était merveilleuse. Les hommes sont
 souvent aveuglés par la richesse et la cupidité.
\ No newline at end of file
+souvent aveuglés par la richesse et la cupidité.
+
+Auteurs: Adrien Hélios, et Killian Nachiar
```

### ajouts des auteurs - a nouveau

```
@@ -28,4 +28,4 @@ Ne vous fiez jamais aux apparences, le pêcheur est mort en
 croyant que cette clé était merveilleuse. Les hommes sont
 souvent aveuglés par la richesse et la cupidité.
 
-Auteurs: Lisa Orange et Damien Dennemont
+Auteurs: Lisa Orange et Damien Dennemont, Adrien Hélios, et Killian Nachiar
```