# Introduction à GIT

---

## Git: pourquoi ?

Pour collaborer autour d'un document.<!-- .element: class="fragment" data-fragment-index="1" -->

De manière: <!-- .element: class="fragment" data-fragment-index="2" -->
- décentralisée <!-- .element: class="fragment" data-fragment-index="3" -->
- historisée <!-- .element: class="fragment" data-fragment-index="4" -->
- modulaire <!-- .element: class="fragment" data-fragment-index="5" -->

---

## Git: Comment ?

La base avec une mise en situation. Vous collaborez avec un auteur à la 
rédaction d'un petit conte.

--

### Comment, récupérer un document ?

Comment récupérer l'ensemble des documents pour le consulter,
et les modifier en local ?

Estimons que ces documents sont sur une page web:
<https://gitlab.com/dblaisonneau/initiation-git>

<div class="fragment">
Avec le CLI Git

```sh
git clone https://gitlab.com/dblaisonneau/initiation-git
```
</div>

--

![version1](version1.png)

--

### Comment, modifier un document en local et préparer son partage ?

Le conte n'est pas terminé, ajoutons donc une fin:

> Ne vous fiez jamais aux apparences, le pêcheur est mort en
> croyant que cette clé était merveilleuse. Les hommes sont
> souvent aveuglés par la richesse et la cupidité.

Mais comment voir ce qui a été modifié ?

--

### Comment, observer les modifications ?

Comment peut on observer ce qui a été modifié:

- `git status`
- `git diff`

--

#### Comment, savoir quel fichier a été modifié ?

```sh
git status

Sur la branche main
Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git restore <fichier>..." pour annuler les modifications dans le répertoire de travail)
	modifié :         conte-le-pecheur-et-le-geant.txt

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")
```

--

#### Comment connaitre la différence avec l'original

```sh
git diff
```

![diff1](diff1.png)

--

### Comment, préparer une nouvelle version

Une fois qu'un document est prêt, on prépare une nouvelle version:

On indique quel fichier prendre en compte:

```sh
git add conte-le-pecheur-et-le-geant.txt
```

<div class="fragment">
On indique pourquoi on a modifié le document, avec un titre et une description:

```sh
git commit

    Ajout de la fin du conte
    
    Il manquait la fin du conte 'le pecheur et le geant'. Le conte
    est maintenant terminé.
```
</div>

--

### Comment: Partager la modification ?

On pousse la modification sur le référentiel commun.

```sh
git push

Énumération des objets: 5, fait.
Décompte des objets: 100% (5/5), fait.
Compression par delta en utilisant jusqu'à 12 fils d'exécution
Compression des objets: 100% (3/3), fait.
Écriture des objets: 100% (3/3), 491 octets | 491.00 Kio/s, fait.
Total 3 (delta 2), réutilisés 0 (delta 0), réutilisés du pack 0
To git+ssh://gitlab.com/dblaisonneau/initiation-git.git
```

--

### Qu'est ce qui a été partagé ?

```sh
git log -p -n1
Author: David Blaisonneau <***************>
Date:   Wed Jun 7 15:12:04 2023 +0200

    Ajout de la fin du conte
    
    Il manquait la fin du conte 'le pecheur et le geant'. Le conte
    est maintenant terminé.

diff --git a/conte-le-pecheur-et-le-geant.txt b/conte-le-pecheur-et-le-geant.txt
index 0a98275..3d5c1b4 100644
--- a/conte-le-pecheur-et-le-geant.txt
+++ b/conte-le-pecheur-et-le-geant.txt
@@ -22,4 +22,8 @@ fort.
 
 Il vit une maison. Il entra et vit un géant.
 
-Le géant le mangea. C'est ainsi que le pêcheur mourut.
\ No newline at end of file
+Le géant le mangea. C'est ainsi que le pêcheur mourut.
+
+Ne vous fiez jamais aux apparences, le pêcheur est mort en
+croyant que cette clé était merveilleuse. Les hommes sont
+souvent aveuglés par la richesse et la cupidité.
\ No newline at end of file
```

--

## Comment: récupérer la dernière version ?

On demande au repository la dernière version

```bash
git pull
```

---

## Collaborons à plus large échelle

Trois personnes vont maintenant modifier le document:

1. Remplacons `CHOUCROUTE` par `île`
1. Ajoutons les auteurs: Lisa Orange et Damien Dennemont
1. Ajoutons les auteurs: Adrien Hélios, et Killian Nachiar

Comment ?

- modification du fichier
- `git add`
- `git commit`
- `git push`

--

### Pour le premier, pas de soucis

```
❯❯❯  git add conte-le-pecheur-et-le-geant.txt

❯❯❯  git commit -m "correction du lieu d'échouage"

[main 6222a1e] correction du lieu d'échouage
 1 file changed, 1 insertion(+), 1 deletion(-)

❯❯❯  git push

Énumération des objets: 5, fait.
Décompte des objets: 100% (5/5), fait.
...
   f11382d..6222a1e  main -> main
```

--

### Pour le second, c'est pas aussi simple

On ne peut pas pousser le document, nous n'avons pas la dernière version.

```bash
❯❯❯  git push                                                                                                                             ⎈ (arn:aws:eks:eu-west-3:390623597627:cluster/pikeo-dev-main-casa-cluster/flux-system)
To git+ssh://gitlab-perso.com/dblaisonneau/initiation-git.git
 ! [rejected]        main -> main (non-fast-forward)
erreur : impossible de pousser des références vers 'git+ssh://gitlab-perso.com/dblaisonneau/initiation-git.git'
astuce: Les mises à jour ont été rejetées car la pointe de la branche courante est derrière
astuce: son homologue distant. Intégrez les changements distants (par exemple 'git pull ...')
astuce: avant de pousser à nouveau.
astuce: Voir la 'Note à propos des avances rapides' dans 'git push --help' pour plus d'information.

❯❯❯  git pull                                                                                                                             ⎈ (arn:aws:eks:eu-west-3:390623597627:cluster/pikeo-dev-main-casa-cluster/flux-system)
Rebasage et mise à jour de refs/heads/main avec succès.

❯❯❯  git push
   6222a1e..9624ae1  main -> main
```

--

### Pour le troisième, c'est plus compliqué

Ici aussi le document a évolué par rapport à notre référence.

```
❯❯❯  git push
To git+ssh://gitlab-perso.com/dblaisonneau/initiation-git.git
 ! [rejected]        main -> main (non-fast-forward)
erreur : impossible de pousser des références vers 'git+ssh://gitlab-perso.com/dblaisonneau/initiation-git.git'
astuce: Les mises à jour ont été rejetées car la pointe de la branche courante est derrière
astuce: son homologue distant. Intégrez les changements distants (par exemple 'git pull ...')
astuce: avant de pousser à nouveau.
astuce: Voir la 'Note à propos des avances rapides' dans 'git push --help' pour plus d'information.

❯❯❯  git pull
Fusion automatique de conte-le-pecheur-et-le-geant.txt
CONFLIT (contenu) : Conflit de fusion dans conte-le-pecheur-et-le-geant.txt
erreur : impossible d'appliquer 791a61d... ajouts des auteurs
astuce: Resolve all conflicts manually, mark them as resolved with
astuce: "git add/rm <conflicted_files>", then run "git rebase --continue".
astuce: You can instead skip this commit: run "git rebase --skip".
astuce: To abort and get back to the state before "git rebase", run "git rebase --abort".
Impossible d'appliquer 791a61d... ajouts des auteurs
```

Quelqu'un a déjà modifié notre ligne !!! Il y a conflit.

--

### Nous devons résoudre le conflit à la main

On peut voir que le fichier a été modifié:
```
<<<<<<< HEAD
Auteurs: Lisa Orange et Damien Dennemont
=======
Auteurs: Adrien Hélios, et Killian Nachiar
>>>>>>> 791a61d (ajouts des auteurs)
```

Effectuons les corrections:
```
Auteurs: Lisa Orange et Damien Dennemont, Adrien Hélios, et Killian Nachiar
```

--

### Proposons la nouvelle version

```bash
❯❯❯ git add conte-le-pecheur-et-le-geant.txt

❯❯❯ git rebase --continue
[HEAD détachée 4bdb98a] ajouts des auteurs
 1 file changed, 1 insertion(+), 1 deletion(-)
Rebasage et mise à jour de refs/heads/main avec succès.

❯❯❯  git push
Énumération des objets: 5, fait.
Décompte des objets: 100% (5/5), fait.
Compression par delta en utilisant jusqu'à 12 fils d'exécution
Compression des objets: 100% (3/3), fait.
Écriture des objets: 100% (3/3), 336 octets | 336.00 Kio/s, fait.
Total 3 (delta 2), réutilisés 0 (delta 0), réutilisés du pack 0
To git+ssh://gitlab-perso.com/dblaisonneau/initiation-git.git
   9624ae1..4bdb98a  main -> main
```

---

## Voyons notre historique de document

```
❯❯❯  git log -graph --pretty=format:'%h - %d %s' --abbrev-commit
* 4bdb98a -  (HEAD -> main, origin/main) ajouts des auteurs
* 9624ae1 -  ajout des auteurs
* 6222a1e -  correction du lieu d'échouage
* f11382d -  Ajout de la fin du conte
* 10ec98f -  add documentation
* 573810d -  add-conte
* 3cd0177 -  Initial commit
```

---

## Mieux travailler en collaboratif

Jusqu'ici, chacun fait ce qu'il veut, sans en parler aux autres... comment
mieux faire ?

- utiliser une `branch`: c'est version spécifique des documents, souvent liés
  à une demande en particulier, ou un besoin clair (souvent décrit dans un
  ticket, ou un `issue`)
- une fois que cette branche est faite, elle est "proposée" à la communauté via
  un `Merge Request` sur Gitlab ou un `Pull Request` sur GitHub.

Voyons ca en pratique.

---

## Quelques avantages

Grâce à Git, et à GitLab, GitHub on profite de:

- un historique des versions de nos documents
- une meilleure collaboration
- une revue de document avant fusion
- d'automatisation des tâches (vérification syntaxique, build, ...)
